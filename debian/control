Source: python-dicompylercore
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3,
               python3-setuptools,
               python3-matplotlib <!nocheck>,
               python3-pydicom <!nocheck>,
               python3-shapely <!nocheck>,
               python3-skimage <!nocheck>
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/python-dicompylercore
Vcs-Git: https://salsa.debian.org/med-team/python-dicompylercore.git
Homepage: https://github.com/dicompyler/dicompyler-core
Rules-Requires-Root: no

Package: python3-dicompylercore
Architecture: any
Depends: ${python3:Depends},
         ${shlibs:Depends},
         ${misc:Depends},
         python3-numpy,
         python3-pydicom,
         python3-matplotlib,
         python3-six,
         python3-pil,
         python3-shapely,
         python3-skimage
Description: core radiation therapy modules for DICOM / DICOM RT used by dicompyler
 This package provides a Python3 library of core radiation therapy
 modules for DICOM / DICOM RT used by dicompyler. It includes:
 .
  * dicomparser: parse DICOM objects in an easy-to-use manner
  * dvh: Pythonic access to dose volume histogram (DVH) data
  * dvhcalc: Independent DVH calculation using DICOM RT Dose & RT Structure Set
